import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { HelperService } from '../_helpers/helper.service';
import { environment } from '../../../environments/environment';
import { 
  CountriesInterface, 
  DialCodesInterface,
  ProductCategory,
  ProductIndustry,
  ProductOperatingSystem
} from '../_interfaces/Enums';

@Injectable()
export class EnumsService {
  private apiRoot: string;
  private productDictionaries: string = 'Products/dictionaries/';

  constructor(private http: HttpClient, private help: HelperService) { 
    this.apiRoot = this.help.getApiUrl();
  }

  // Gets list of countries with dialing codes
  public getListOfCountries(): Observable<Array<CountriesInterface>> {
    let url = `${this.apiRoot}Countries`;

    return this.http.get<CountriesInterface[]>(url);
  }

  // Only for Empire API
  // Rebel API contains dialing codes within list of countries
  public getListOfDialingCodes(): Observable<Array<DialCodesInterface>> {
    let url = `${environment.apiUrl}CountryInternationalCallingCodes`;

    return this.http.get<DialCodesInterface[]>(url);
  }

  // =====================================================================
  // ===================== Enums for product filters= ====================

  public getProductCategories(): Observable<Array<ProductCategory>> {
    return this.http.get<ProductCategory[]>(
      `${this.apiRoot}${this.productDictionaries}categories`,
    );
  }

  public getProductIndustries(): Observable<Array<ProductIndustry>> {
    return this.http.get<ProductIndustry[]>(
      `${this.apiRoot}${this.productDictionaries}industries`
    );
  }

  public getProductOperatingSystems(): Observable<Array<ProductOperatingSystem>> {
    return this.http.get<ProductOperatingSystem[]>(
      `${this.apiRoot}${this.productDictionaries}os`
    );
  }

  // ===================== Enums for product filters= ====================
  // =====================================================================
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { HelperService } from '../_helpers/helper.service';
import { Product } from '../_interfaces/Products';

@Injectable()
export class ProductsService {
  private apiRoot = '';
  private productsPath = '';

  constructor(private http: HttpClient, private help: HelperService) { 
    this.apiRoot = this.help.getApiUrl();
    this.productsPath = `${this.apiRoot}Products`
  }

  // List all the product - not filtered
  public getListOfProducts() {
    return this.http.get<Array<Product>>(
      `${this.productsPath}`
    );
  }

  // Gets product detail by ID
  public getProduct(productId: number) {
    return this.http.get<Product>(
      `${this.productsPath}/${productId}`
    );
  }

  // Adds new product
  public addNewProduct(product: Product) {
    return this.http.post(
      `${this.productsPath}`,
      { ...product },
      { observe: 'response' }
    );
  }

  // Updates selected product
  public updateProduct(productId: number, product: Product) {
    return this.http.put(
      `${this.productsPath}/${productId}`,
      { ...product },
      {observe: 'response'}
    );
  }

  // Deletes selected product
  public deleteProduct(productId: number) {
    return this.http.delete(
      `${this.productsPath}/${productId}`,
      {observe: 'response'}
    );
  }

  // =====================================================================
  // ==================== Uploaded product operations ====================
  
  public approveProduct(productId: number) {
    return this.http.post(
      `${this.productsPath}/${productId}/approve`,
      {observe: 'response'}
    );
  }

  public deployProduct(productId: number) {
    return this.http.post(
      `${this.productsPath}/${productId}/deploy`,
      {observe: 'response'}
    );
  }

  public publishProduct(productId: number) {
    return this.http.post(
      `${this.productsPath}/${productId}/publish`,
      {observe: 'response'}
    );
  }

  public purchaseProduct(productId: number) {
    return this.http.post(
      `${this.productsPath}/${productId}/purchase`,
      {observe: 'response'}
    );
  }

  public rateProduct(productId: number) {
    return this.http.post(
      `${this.productsPath}/${productId}/rate`,
      {observe: 'response'}
    );
  }

  public addProductToWishlist(productId: number) {
    return this.http.post(
      `${this.productsPath}/${productId}/wishlist`,
      {observe: 'response'}
    );
  }
  // ==================== Uploaded product operations ====================
  // =====================================================================  

}

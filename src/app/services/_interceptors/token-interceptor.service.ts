import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/do';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  intercept
  (
    req: HttpRequest<any>,
    next: HttpHandler
  )
  : Observable<HttpEvent<any>> 
  {
    const accessToken = sessionStorage.getItem('accessToken');

    if(accessToken && accessToken !== '') {
      const clone = req.clone({
        setHeaders: {
          ['Authorization']: `Bearer ${accessToken}`
        }
      });
      return next.handle(clone);
    } else {
      return next.handle(req);
    }
  }
  constructor() { }

}

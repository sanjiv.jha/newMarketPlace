import { Injectable } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { LoginInterface } from '../_interfaces/Login';
import { HelperService } from '../_helpers/helper.service'; // Used to decide which API to use
import { environment } from '../../../environments/environment';
import "rxjs/add/operator/map";
import { OrganizationDetails, IndividualDetails, CompleteIndividualForm, ResendEmailResponse } from '../_interfaces/Accounts';

@Injectable()
export class RegisterService {
  public accessToken: string;
  private restItems: any;
  private result: Array<Object>;
  private apiRoot: string
  
  constructor(private http: HttpClient, private help: HelperService)  { 
    // TODO: Remove once API is decided
    this.apiRoot = this.help.getApiUrl();
  }

  public signup(value: any): Observable<any> {
    // TODO: Remove once API is decided
    let regPath = this.help.getApiPaths();
    let url = `${this.apiRoot}${regPath.register}`;   /// remove apiRoot once it is on server
    let registrationParams = this.help.getRegObject(value.userName, value.password);
    // TODO: Remove once API is decided

    return this.http.post(url, 
      registrationParams,
      { observe: 'response' }
    )
  }

  public registerOrganization(busDetails: OrganizationDetails): Observable<any> {
    return this.http.post(
      `${this.apiRoot}Companies`, 
      busDetails, 
      {observe: 'response'}
    );
  }

  public registerIndividual(indivDetails: CompleteIndividualForm): Observable<any> {
    let userId = sessionStorage.getItem('userId');
    return this.http.patch(
      `${this.apiRoot}Accounts/${userId}/UpdateProfile`, 
      indivDetails, 
      {observe: 'response'}
    );
  }

  public setRegistrationEmail(email: string): void {
    sessionStorage.setItem('emailAddress', email);
  }

  public resendEmail(email: string) {
    this.http.post<ResendEmailResponse>(
      `${this.apiRoot}Users/ResendVerificationEmail`, 
      {'emailAddress': email},
      { observe: 'response'}
    ).subscribe(res => {
        console.log('Resend response:', res);
        if (String(res.status) === 'Executed') {
          console.log('Email resent Successful');
        }
        else {
          console.log('Operation returned unknown answer!');
        }
      },
      err => {
        console.log('Operation failed:', err);
      }
    );
  }

  public verifyEmail(userId: string, code: string) {
    return this.http.post(
      `${this.apiRoot}Accounts/ConfirmEmail`,
      {'userId': userId, 'code': code},
      { observe: 'response'}
    );
  }
}

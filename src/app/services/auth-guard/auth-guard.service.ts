import { Injectable } from '@angular/core';
import { CanActivate,
        Router,
        ActivatedRouteSnapshot,
        RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../auth/auth.service';

@Injectable()
export class AuthGuardService implements CanActivate{

  constructor(private auth: AuthService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    let url: string = state.url;

    return this.checkLogin(url);
  }

  checkLogin(url: string): boolean {
    // Can navigate if user is logged in
    if (this.auth.isLoggedIn()) {
      return true;
    }

    this.auth.redirectUrl(url);

    this.router.navigateByUrl("/login");
    return false;
  }
}

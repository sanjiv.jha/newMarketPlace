import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import "rxjs/add/operator/map";
import { catchError } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { environment } from '../../../environments/environment';
import { HelperService } from '../_helpers/helper.service';
import { LoginInterface } from '../_interfaces/Login';

@Injectable()
export class AuthService {
  private loggedIn: boolean;
  private urlToRedirect: string;
  public observeLogin: BehaviorSubject<boolean>;

  constructor(private http: HttpClient, private help: HelperService) { 
    this.loggedIn = !!sessionStorage.getItem('accessToken');
    this.observeLogin = new BehaviorSubject<boolean>(this.loggedIn);
  }

  public isLoggedIn(): boolean {
    return this.loggedIn;
  }

  public setToken(token: string): void {
    sessionStorage.setItem('accessToken', token);
    this.loggedIn = true;
    this.observeLogin.next(this.loggedIn);
  }

  public setUser(userId: string): void {
    sessionStorage.setItem('userId', userId);
  }

  public getToken(): string {
    return sessionStorage.getItem('accessToken');
  }

  // Getter/Setter function
  public redirectUrl(url?: string) {
    if(url !== undefined && url.length > 0) {
      this.urlToRedirect = url;
    } else {
      return this.urlToRedirect;
    }
  }

  public logout(): void {
    this.loggedIn = false;
    this.observeLogin.next(this.loggedIn);
    sessionStorage.removeItem('accessToken');
    sessionStorage.removeItem('currentUser');
  }

  public login(userName: string, password: string) {
    // TODO: Remove once API is decided
    let apiRoot = this.help.getApiUrl();
    let regPath = this.help.getApiPaths();
    let url = `${apiRoot}${regPath.login}`;
    // TODO: Remove once API is decided

    return this.http.post<LoginInterface>(
      `${apiRoot}Accounts/Login`,
      { userName: userName, password: password},
      { observe: 'response'}
    );
  }
}


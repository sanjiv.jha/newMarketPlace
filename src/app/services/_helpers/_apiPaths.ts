export const empirePaths = {
  register: 'Users/',
  login: 'Accounts/Login'
}

export const rebelPaths = {
  register: 'Accounts/Register',
  login: 'Accounts/Login',
}
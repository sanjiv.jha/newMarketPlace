import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

import { empirePaths, rebelPaths} from './_apiPaths';
import { empireRegister, rebelRegister } from './_apiObjects';

@Injectable()
export class HelperService {
  private apiRoot: string = '';
  private isRebel: boolean;

  constructor() {
    let usedApi = localStorage.getItem('usedApi');
    if (usedApi === null) {
      localStorage.setItem('usedApi', 'rebel');
      this.isRebel = true;
    } else {
      this.isRebel = usedApi === 'rebel';
    }
  }

  getApiUrl(): string {
    this.apiRoot = this.isRebel
                   ? 
                   environment.rebelApiUrl 
                   : 
                   environment.apiUrl;
    return this.apiRoot;
  }

  getApiPaths() {
    return this.isRebel ? rebelPaths : empirePaths;
  }

  getRebelFlag(): boolean {
    return this.isRebel;
  }

  setRebelFlag(isRebel?: boolean): void {
    console.log(`Changing api from ${this.isRebel ? 'Rebel' : 'Default'} to ${this.isRebel ? 'Default' : 'Rebel'}`);
    this.isRebel = isRebel;
    localStorage.setItem('usedApi', this.isRebel ? 'rebel' :'default');
  }

  getRegObject(userName: string, password: string) {
    if(this.isRebel) { 
      let regObj = rebelRegister;

      regObj.confirmPassword = password;
      regObj.password = password;
      regObj.email = userName;

      return regObj;
    } else { 
      let regObj = empireRegister;

      return {...regObj, password, userName}
    }
  }
}

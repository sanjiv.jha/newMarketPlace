export interface Product {
  approvalStatus?: string,
  currency: string,
  description: string,
  featured?: true,
  inserted?: string | Date,
  operatingSystemId: number,
  price: number,
  privacyText: string,
  productId?: number,
  published?: true,
  rating?: number,
  requiredMemoryAmount: number,
  termsOfServiceText: string,
  title: string,
  updated?: string | Date
}
export interface Address {
  city: string,
  countryId: number,
  region: string,
  street: string,
  streetNumber: string,
  zipCode: string
};

export interface OrganizationDetails {
  address: Address,
  dialingCode: string,
  dunsId?: string,
  name: string,
  phoneNumber: string,
  regNo: string,
  vatNo: string,
  webAddress: string,
  zipCode?: string
};

export interface IndividualDetails {
  address: Address | Array<Address>,
  companyId: string,
  email: string,
  emailConfirmed: boolean,
  firstName: string,
  id: string,
  isMainContact: true,
  lastName: string,
  phoneNumber: string,
  phoneNumberConfirmed: string,
  userName: string
};

export interface CompleteIndividualForm {
  address: Address | Array<Address>,
  companyId?: string,
  firstName: string,
  lastName: string,
  phoneNumber?: string
};

export interface ResendEmailResponse {
  notifications?: Array<any>,
  relatedCommandId?: string,
  status: string,
};

export interface PasswordChange {
  currentPassword: string;
  newPassword: string;
}
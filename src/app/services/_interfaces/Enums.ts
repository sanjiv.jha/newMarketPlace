export interface CountriesInterface {
  code: string,
  description: string,
  id: number,
  dialingCode?: string
};

export interface DialCodesInterface {
  code: string,
  country: {
    code: string,
    description: string,
    id: number
  }
  countryId: string,
  id: number
}

export interface ProductCategory {
  id: number,
  parentId: number,
  value: string
}

export interface ProductIndustry {
  id: number,
  parentId: number,
  value: string
}

export interface ProductOperatingSystem {
  id: number,
  operatingSystemVersion: string,
  platform: string
}

export interface LoginInterface {
  access_token: string;
  expiration: string | Date;
  serverDate: string | Date;
  userId: string;
}
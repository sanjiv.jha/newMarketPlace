import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { HelperService } from '../_helpers/helper.service';
import { 
  PasswordChange 
} from '../_interfaces/Accounts';

@Injectable()
export class AccountsService {
  private accountsPath: string;

  constructor(private http: HttpClient, private help: HelperService) { 
    this.accountsPath = `${this.help.getApiUrl()}Accounts/`;
  }

  public getCurrentUser() {
    return this.http.get(
      `${this.accountsPath}Current`
    );
  }

  public updateUserProfile(userId: string, user: Object) {
    return this.http.patch(
      `${this.accountsPath}${userId}/UpdateProfile`,
      { ...user },
      { observe: 'response' }
    );
  }

  public changeUserPassword(passChange: PasswordChange) {
    return this.http.post(
      `${this.accountsPath}ChangePassword`,
      passChange,
      { observe: 'response' }
    );
  }


}

import { Component, OnInit } from '@angular/core';

import { UserblockService } from './userblock.service';
import { AuthService } from '../../../services/auth/auth.service';

@Component({
    selector: 'app-userblock',
    templateUrl: './userblock.component.html',
    styleUrls: ['./userblock.component.scss']
})
export class UserblockComponent implements OnInit {
    user: any;
    isLoggedIn: boolean;

    constructor(public userblockService: UserblockService,
                private auth: AuthService) {

        this.user = {
            picture: 'assets/img/user/01.jpg'
        };
    }

    ngOnInit() {
        this.isLoggedIn = this.auth.getToken() !== null;
    }

    userBlockIsVisible() {
        return this.userblockService.getVisibility();
    }

}

import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { SettingsService } from '../../core/settings/settings.service';
import { AuthService } from '../../services/auth/auth.service';
import { HelperService } from '../../services/_helpers/helper.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    loginSubscribtion: Subscription;
    isLoggedIn: boolean;
    isRebel: boolean;

    constructor(public settings: SettingsService,
                private auth: AuthService,
                private router: Router,
                private help: HelperService
    ) { }

    ngOnInit() {
        this.isRebel = this.help.getRebelFlag();
        this.loginSubscribtion = this.auth.observeLogin.subscribe(item => this.isLoggedIn = item);
        console.log('isRebel:', this.isRebel, 'usedApi:', localStorage.getItem('usedApi'));
    }

    toggleApi(): void {
        this.isRebel = !this.isRebel;
        this.help.setRebelFlag(this.isRebel);
    }

    logout(): void {
        console.log('Logout')
        this.auth.logout();
        this.router.navigate(['/login']);
    }
}

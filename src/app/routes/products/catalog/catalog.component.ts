import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ProductsService } from '../../../services/products/products.service';
import { Product } from '../../../services/_interfaces/Products';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.scss']
})
export class CatalogComponent implements OnInit {
  public isWorking: boolean = false;
  public listOfProducts: Array<Product>;

  constructor(private products: ProductsService, private router: Router) { }

  ngOnInit() {
    this.products.getListOfProducts().subscribe(
      res => {
        this.listOfProducts = res;
      },
      err => {
        console.log('Error:', err);
      }
    );
  }

  public selectProduct(productId: number): void {
    console.log('ProductId:', productId);
    this.router.navigate(['product', productId]);
  }

  public updateProduct(): void {
    let prod: Product = {
      currency: 'EUR',
      description: 'Even shorter description than short description of the product',
      operatingSystemId: 1,
      price: 25,
      privacyText: 'Not sure what to put to this privacy text field',
      requiredMemoryAmount: 15,
      termsOfServiceText: 'Not sure about this field either. But lets say MIT',
      title: 'Super Blockchainator 3001 Turbo Deluxe'
    };
    this.products.updateProduct(8, prod).subscribe(
      res => {
        console.log('Update response:', res);
      },
      err => {
        console.log('Update error', err);
      }
    );
  }

}

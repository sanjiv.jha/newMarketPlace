import { Component, OnInit, Input } from '@angular/core';

import { Product } from '../../../services/_interfaces/Products';

@Component({
  selector: 'product-overview',
  templateUrl: './product-overview.component.html',
  styleUrls: ['./product-overview.component.scss']
})
export class ProductOverviewComponent implements OnInit {
  @Input() product: Product;

  public currency: string = '€';

  constructor() { }

  ngOnInit() {
  }

}

import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from '../../services/auth-guard/auth-guard.service';

import { CatalogComponent } from './catalog/catalog.component';
import { ProductOverviewComponent } from './product-overview/product-overview.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';

const routes: Routes = [
  { path: '', redirectTo: 'catalog', pathMatch: 'full' },
  { path: 'catalog', component: CatalogComponent },
  { path: 'product', component: CatalogComponent },
  { path: 'product/:id', component: ProductDetailComponent },
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    CatalogComponent,
    ProductOverviewComponent,
    ProductDetailComponent
  ],
  exports: [
    RouterModule
  ]
})
export class ProductsModule { }

import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../../core/settings/settings.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { AuthService } from '../../../services/auth/auth.service';
import { LoginInterface } from '../../../services/_interfaces/Login';
import { Router } from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    // private loginResp: LoginInterface;
	private accessToken: string;
	public isWorking: boolean = false;
	public valForm: FormGroup;
	public logError: boolean =  false;
	public errorMsg: string = '';

	constructor(public settings: SettingsService, fb: FormBuilder, private auth: AuthService, private router: Router) {
		this.valForm = fb.group({
			'email': [null, 
				Validators.compose([
					Validators.required, 
					CustomValidators.email
				])
			],
			'password': [null, Validators.required]
		});
	}
	
	ngOnInit() { }

	submitForm($ev, value: any) {
		$ev.preventDefault();	
		for (let c in this.valForm.controls) {
			this.valForm.controls[c].markAsTouched();
		}
		if (this.valForm.valid) {
			this.auth.login(value.email, value.password)
			.subscribe(resp => 
				{
					let loginResp: LoginInterface = {...resp.body};
					if(resp.status === 200 && loginResp.access_token) {
						this.processError(false);
						this.auth.setToken(loginResp.access_token);
						this.auth.setUser(loginResp.userId);
						this.router.navigate(['/']);
					} else {
						console.log('Missing token or unkonwn status');
					}
				},
				err => {
					this.processError(true, err.error)
					console.log('Login failed:', err);
				}
			);
		}
	}

	private processError(hasError: boolean, errorMsg?: string): void {
		this.logError = hasError;
		this.errorMsg = (!!errorMsg && errorMsg.length) > 0 ? errorMsg : '';
	}
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegcompleteComponent } from './regcomplete.component';

describe('RegcompleteComponent', () => {
  let component: RegcompleteComponent;
  let fixture: ComponentFixture<RegcompleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegcompleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegcompleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

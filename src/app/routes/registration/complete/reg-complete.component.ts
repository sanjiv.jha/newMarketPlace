import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SettingsService } from '../../../core/settings/settings.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';

import { EnumsService } from '../../../services/_enumservices/enums.service';
import { RegisterService } from '../../../services/register/register.service';
import { Address, OrganizationDetails, CompleteIndividualForm } from '../../../services/_interfaces/Accounts';

@Component({
  selector: 'reg-complete',
  templateUrl: './reg-complete.component-new.html',
  styleUrls: ['./reg-complete.component.scss']
})

export class RegCompleteComponent implements OnInit {
  valForm: FormGroup;
  orgForm: FormGroup;
  result: Array<Object>;
  countries: any;
  callingcodes:any;
  businessCardVisible: boolean = false;

  constructor(public settings: SettingsService, fb: FormBuilder, private router: Router,
              private enums: EnumsService, private regService: RegisterService ) {

    this.valForm = fb.group({
      'firstName': [null],
      'lastName': [null],
      'phoneNumber': [null],
      'emailAddress': [null],
      'phoneNumberCode': [null],
      'city': [null],
      'countryId': [null],
      'region': [null],
      'street': [null],
      'streetNumber': [null],
      'zipCode': [null],
      'webAddress': [null],
      'orgName': [null],
      'regNumber': [null],
      'vatNumber': [null],
      'orgStreet': [null],
      'orgCity': [null],
      'orgStreetNumber': [null],
      'orgCountryId': [null],
      'orgZipCode': [null],
      'orgRegion': [null]
    });
  }

  ngOnInit() {
    this.initEnums();
  }

  public toggleBusinessSection(): void {
    this.businessCardVisible = !this.businessCardVisible;
  }

  private initEnums(): void {
     this.enums.getListOfCountries().subscribe(
      response => {
        this.countries = response;
      },
      error => {
        console.log('Failed to load list of countries:', error);
      }
    );
  }

  submitForm($ev, value: any) {
    $ev.preventDefault();

    if (this.businessCardVisible === true) {
      this.regService.registerOrganization({
        'webAddress': value.webAddress,
        'name': value.orgName,
        'regNo': value.regNumber,
        'vatNo': value.vatNumber,
        'phoneNumber': value.phoneNumber,
        'dialingCode': value.phoneNumberCode,
        'zipCode': value.orgZipCode,
        'address': 
          {
            'countryId': value.orgCountryId,
            'region': value.orgRegion,
            'city': value.orgCity,
            'street': value.orgStreet,
            'streetNumber': value.orgStreetNumber,
            'zipCode': value.orgZipCode
          }
        }
      )
      .subscribe(
        res => console.log(res),
        msg => console.error(`Error: ${msg.status} ${msg.statusText}`)
      );
    }
    
    this.regService.registerIndividual(
      {
        'firstName': value.firstName,
        'lastName': value.lastName,
        "address": {
          'countryId': value.orgCountryId,
          'region': value.region,
          'city':value.city,
          'street': value.street,
          'streetNumber': value.streetNumber,
          'zipCode': value.zipCode,
        }
      }
    )
    .subscribe(
      res => {
        console.log(res);
        this.router.navigateByUrl('/home');
      }, 
      msg => console.error(`Error: ${msg.status} ${msg.statusText}`)
    );
  }
}

import { Component, OnInit} from '@angular/core';
import { SettingsService } from '../../../core/settings/settings.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { Router } from '@angular/router';
import { RegisterService } from '../../../services/register/register.service';
declare var $: any;

@Component({
    selector: 'reg-initial',
    templateUrl: './reg-initial.component.html',
    styleUrls: ['./reg-initial.component.scss']
})

export class RegInitialComponent implements OnInit {
    valForm: FormGroup;
    passwordForm: FormGroup;
    hasEmailError: boolean = false;
    hasPasswordError: boolean = false;
    errorMsg: Array<string> = [];

    constructor(public settings: SettingsService, fb: FormBuilder, private register: RegisterService, private router: Router) {
      let password = new FormControl('', Validators.compose([
          Validators.required, 
          Validators.pattern('^[a-zA-Z0-9]{6,10}$')
        ])
      );
      let certainPassword = new FormControl('', [Validators.required, CustomValidators.equalTo(password)]);

      this.passwordForm = fb.group({
          'password': password,
          'confirmPassword': certainPassword
      });

      this.valForm = fb.group({
          'userName': [null, Validators.compose([Validators.required, CustomValidators.email])],
          'isOrganizationRepresentative': [null],
          'password': [null] // , Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z0-9]{6,10}$')])]
      });
    }

    ngOnInit() { }

    submitForm($ev, value: any) {
      $ev.preventDefault();
      for (let c in this.valForm.controls) {
        this.valForm.controls[c].markAsTouched();
      }

      if (this.valForm.valid) {
        this.register.signup(value).subscribe(
          resp => {
            console.log('Registration response:', resp);
            if(resp.status === 200)
            {
              this.processError(false);
              this.register.setRegistrationEmail(value.userName);
              this.router.navigateByUrl('/register/pending');
            }
            else
            {
              console.log('Failed');
            }
          },
          err => {
            this.processError(true, err.error);
            console.log('Registration failed:', err);
          }
        );
      }
    }

    private processError(hasError: boolean, errorMsg?: Object): void {
      this.hasEmailError = this.hasPasswordError = false;
      // Clears the error flags and messages
      if (!hasError && errorMsg === undefined) {

        this.errorMsg = [];
        return;
      }

      if(errorMsg !== undefined) {
        this.errorMsg = errorMsg[Object.keys(errorMsg)[0]];
        console.log('Includes password:', this.errorMsg.includes('Password'))
        // TODO: Risky code - finde better way to determine error message type
        if(this.errorMsg.length > 1) {
          this.hasPasswordError = true;
        } else {
          this.hasEmailError = true;
        }
      }
    }
}

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { RegisterService } from '../../../services/register/register.service';
import { SettingsService } from '../../../core/settings/settings.service';

@Component({
  selector: 'reg-verify',
  templateUrl: './reg-verify.component.html',
  styleUrls: ['./reg-verify.component.scss']
})
export class RegVerifiedComponent implements OnInit {
  private userId: string;
  private verCode: string;
  public isWaiting: boolean = false;
  public resultMsg: string = '';
  
  constructor(
    private route: ActivatedRoute, 
    private router: Router,
    private register: RegisterService,
    public settings: SettingsService
  ) {  }

  ngOnInit() {
    let paramMap = this.route.queryParams.subscribe(params => {
      console.log('Params:', params);
      this.userId = params['userId'];
      this.verCode = params['code'];

      this.isWaiting = true;
      this.register.verifyEmail(this.userId, this.verCode).subscribe(
        res => {
          this.isWaiting = false;
          if (res.status === 200) {
            this.resultMsg = 'Your email address was successfully verfied. Proceed to login page.';
          }
        },
        err => {
          this.isWaiting = false;
          this.resultMsg = 'Something went wrong and we are working on it.';          
        }
      );
    });
  }
}

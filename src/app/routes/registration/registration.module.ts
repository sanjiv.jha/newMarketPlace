import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuardService } from '../../services/auth-guard/auth-guard.service';
import { RegInitialComponent } from './initial/reg-initial.component';
import { RegCompleteComponent } from './complete/reg-complete.component';
import { RegPendingComponent } from './pending/reg-pending.component';
import { RegVerifiedComponent } from './verify/reg-verify.component';

const routes: Routes = [
    { path: '', redirectTo: 'initial', pathMatch: 'full'},
    { path: 'initial', component: RegInitialComponent },
    { path: 'pending', component: RegPendingComponent },
    { path: 'verify', component: RegVerifiedComponent },
    { path: 'verify/:userid/:code', component: RegVerifiedComponent },
    { path: 'complete', component: RegCompleteComponent, canActivate: [AuthGuardService]}    
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    RegInitialComponent,
    RegCompleteComponent,
    RegPendingComponent,
    RegVerifiedComponent
  ],
  exports: [
    RouterModule
  ]
})

export class RegistrationModule { }

import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../../core/settings/settings.service';
import { RegisterService } from '../../../services/register/register.service';


@Component({
  selector: 'reg-pending',
  templateUrl: './reg-pending.component.html',
  styleUrls: ['./reg-pending.component.scss']
})
export class RegPendingComponent implements OnInit {
  
  constructor(public settings: SettingsService, private register: RegisterService) {

  }
  cancelsession() {
    sessionStorage.removeItem('emailAddress');
  }
  
  resendemail() {
    this.register.resendEmail(sessionStorage.getItem('emailAddress'));
  }

  ngOnInit() {

  }

}
